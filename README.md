# 使用tap网卡，让vpn客户端所在子网可以访问vpn服务器所在子网的资源

### 配置

```bash
# 安装openvpn软件包
yum -y install easy-rsa openvpn
# 启用IP包转发
echo 'net.ipv4.ip_forward=1' >> /etc/sysctl.conf && sysctl -p
```

启动服务端

```bash
# 要在日志文件里打时间戳
sed -i 's/ --suppress-timestamps//g' /usr/lib/systemd/system/openvpn-server@.service
systemctl daemon-reload
cat > /etc/openvpn/server/server.conf << 'EOF'
dev tap
proto udp
local 172.16.11.1
port 1194
topology subnet
server 10.200.0.0 255.255.255.0
remote-cert-tls client
user openvpn
group openvpn
persist-tun
persist-key
keepalive 10 60
ping-timer-rem
verb 7
daemon
log-append /var/log/openvpn.log
client-config-dir /etc/openvpn/client
client-to-client
push "route 172.16.11.0 255.255.255.0"
push "persist-tun"
push "persist-key"
<tls-crypt>
#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
371230b610ebb21082c6c9d6b33ce479
0373763e596a207d9505ba82d2f9cd00
02d0afff7a0dad5fb32e04e881215d39
9a707ce8057d7e211b0e528ecbded2f3
7588eaa555cad5e043f9a6ac02243d61
95cce2f3eaa319a77bf41fb48f7a5690
2bc63dc7e0d356c710fd5302ec944d70
ece2d60caee95a9e63dd7240ca570f9d
04ccfbaea207a8c25852e16184aa30f5
72107dd002d20e4df69d62f330bb4953
e8a2c26200f42438b2458114cc630e24
d7812a701a54d1fe963b240997887dce
fa43c6470e6093dd2e08e8bd191fc318
431452e91e04789dc533cbbc70420672
83915d3b2392d886de300146eae8277a
35aa3c8a549b35c08d0ca63183251f84
-----END OpenVPN Static key V1-----
</tls-crypt>
<ca>
-----BEGIN CERTIFICATE-----
MIIDSDCCAjCgAwIBAgIUOn5EavRhh0MjROvEX406VZDUDOkwDQYJKoZIhvcNAQEL
BQAwFTETMBEGA1UEAwwKT1BFTlZQTi1DQTAeFw0yNDAxMDYwMjQxMTFaFw0zNDAx
MDMwMjQxMTFaMBUxEzARBgNVBAMMCk9QRU5WUE4tQ0EwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQDvS15NQpMqFPCSkBbEYSYiYAeXkA8KvAFwDQ6fwgwo
9yctWk6c4AGNy7NG/rsaZjngUywPVftR6YXrWIo+iobPknSocjic/j7vAxCkbZAC
wQkWCtQgp1n3IoPGNB2wZQ7eZSX14xk3GhjsdupH7j7oPuC6hLxcoWEy1RYvoa1R
2OsXg4ng7vQNJaHDk5qyD/pZRv8IudqRRrDbwuTbKD2fmilO7e+BmjCsSs5VocaV
hGnqcb/j2TgesESfPjUMxQz4NxcgU9+Rcx+UQ3yDZwh3OB90A+qKF6sGe0UIVsR1
5M9TuZZCLalZELKhWRO7UxGDBStbdUqz18dl2omrR/mzAgMBAAGjgY8wgYwwHQYD
VR0OBBYEFN5mqGBGtsFPXPNnksgRE700HV0yMFAGA1UdIwRJMEeAFN5mqGBGtsFP
XPNnksgRE700HV0yoRmkFzAVMRMwEQYDVQQDDApPUEVOVlBOLUNBghQ6fkRq9GGH
QyNE68RfjTpVkNQM6TAMBgNVHRMEBTADAQH/MAsGA1UdDwQEAwIBBjANBgkqhkiG
9w0BAQsFAAOCAQEA154qeblSiRR6s2bHD5JmXWZ4s+o8n3H9Gz46XBsq8WAMZjuC
XMlzocrpkFMM8r8rWtsAbGqMAkBf1lLCVGMKIHzayfBpRnJ5VOIFY7aESUqGfqCc
s2JMH9meo69cWnwcNPK4PPrgYAC3+UxV9YrvdwyeQUGvbh2ybIiW1M3j7BXBcGKE
/xq0eXoHrjQu2443YsXnwYmdCe7YAPs8QTp+un6FWWIhSIT6/rvmbW6MsndQTyj/
tXLDnqLUVFfM1l8LGqqpFx8Y8buf/1vMdSstnWi7+X/ZBe5nPDCdjQJUa4cWbkxD
MPj3c7Co9WIrCGW/K1PTY0VqUqzeua6DNSAcJw==
-----END CERTIFICATE-----
</ca>
<cert>
-----BEGIN CERTIFICATE-----
MIIDbjCCAlagAwIBAgIRAMJUQ74NkU13BURxrl4gKr0wDQYJKoZIhvcNAQELBQAw
FTETMBEGA1UEAwwKT1BFTlZQTi1DQTAeFw0yNDAxMDYwMjQyMjRaFw0yNjA0MTAw
MjQyMjRaMBUxEzARBgNVBAMMCnZwbi1zZXJ2ZXIwggEiMA0GCSqGSIb3DQEBAQUA
A4IBDwAwggEKAoIBAQCmINl9m9xER5NnbIPBIySt5Z3tid0XMd37x4gLXSncmz6H
R7kvIC4AyXvrgnMN3/dIPk0dDC7octlC/XRoY0yGP8F/MhLvY+bGO3HzfNuJFTI4
LuwfhPjMLu8ti0q8mEBBCcTbWuXD0hnU4+L7rUmPn5rH43bWQOxqFAK+iJgE2aAq
FT2JI9bIcco2FkldlhLwZ8E9RoYidO08GWzbVLX//V0jbgy6mwhiZly9Oseh2jGp
GVWrQgAT4dmnuNZkcgQfL1A6qQUDE3vingJPwEWiGOhmsHMe+lBAZHNL/Z/17BqF
51C0FQ0OLkjzXTX13VxJtpcCy8LtYctBT4q7P0ZNAgMBAAGjgbgwgbUwCQYDVR0T
BAIwADAdBgNVHQ4EFgQUWWGvR2ohelcpChPdnnuwEQn/868wUAYDVR0jBEkwR4AU
3maoYEa2wU9c82eSyBETvTQdXTKhGaQXMBUxEzARBgNVBAMMCk9QRU5WUE4tQ0GC
FDp+RGr0YYdDI0TrxF+NOlWQ1AzpMBMGA1UdJQQMMAoGCCsGAQUFBwMBMAsGA1Ud
DwQEAwIFoDAVBgNVHREEDjAMggp2cG4tc2VydmVyMA0GCSqGSIb3DQEBCwUAA4IB
AQDkyoWzeBl2z5m+QkXd3J/dAwsYkBqAFdJ9702rimGkhOiucLqRCNCpOv13WE8y
DG9S+jM1xsSkxdrYLFFbJo7Jvly9cy6poXa3G9HoBMALaStusrFmTOWDnSTMv32/
NwaN0xxg074Gzn9nkScqYwytNLdTFdOyVgpuSJWqh3170vZdYbrdI7MSxMYo48hj
lACCUMWDDdmBo4ZXpBf0mFmU9HypfMUWy5rYT9Hb7Z0Mm0xVagnmxi8P6QbqO+h2
bLh8PNv8/OiFAfnIsLwAEgvWsjAh/zYkDni+bMtGmBdPf/Ob14Qz87+e8LErNM+u
31IjoWdJx+Kv+IsWPXQvQOtE
-----END CERTIFICATE-----
</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCmINl9m9xER5Nn
bIPBIySt5Z3tid0XMd37x4gLXSncmz6HR7kvIC4AyXvrgnMN3/dIPk0dDC7octlC
/XRoY0yGP8F/MhLvY+bGO3HzfNuJFTI4LuwfhPjMLu8ti0q8mEBBCcTbWuXD0hnU
4+L7rUmPn5rH43bWQOxqFAK+iJgE2aAqFT2JI9bIcco2FkldlhLwZ8E9RoYidO08
GWzbVLX//V0jbgy6mwhiZly9Oseh2jGpGVWrQgAT4dmnuNZkcgQfL1A6qQUDE3vi
ngJPwEWiGOhmsHMe+lBAZHNL/Z/17BqF51C0FQ0OLkjzXTX13VxJtpcCy8LtYctB
T4q7P0ZNAgMBAAECggEABArTP/Xa645nondG0sbteDhuGf7mHPpe/Ga5Lg2bnH0/
4urL3BRcX4iHi2N73oqPPyLE8uqo+YmnNQoVxbR6LYoU8JNiApExEaxrSWbFsMA4
nfAGWX39enpp4iHRimU/Vash2cnhwrIKDt68rIMQQdO7Cxdt0ijM7TKEyT4USjSM
sDGehnkGjcTX4w3nnvyJ8sQ+T0lhfu7GLUoNYqzcPjS9Bn9z37/kEmptbB2O5yMG
o48AzBCXp2L849/ZyJwYiDecRWuAFYPNkeKcVCf9e9YA1q6/8xrrX1FVzZdi8Sr9
v7FKeS3wg7xjMgheBe0GkvvcvkoTTam96CRWAcV2AQKBgQC+13kQmwpZbC8y48PE
/V9fnAF0ImyWcQD+UlscpcSHHZ0rWNI/oujB8V8K+uY7tbG0CjTQXXqJBuzbe8Kc
IWDZcQBWs30m5FlRVVbQEvvBf8X9ViYwKRZm2iHokscZiE+oOeVi2hIJL9NrBbDd
XUHUgo9lNoSnVW7h/PQbJ36zeQKBgQDe2U2cpdLnSwTk0q1FhFymmS9VGZ3gqNj/
0AVgJVz8ThkKUL09YZSNIoARWdM0saC2yMvZJVC21wVuFjli22F3C3wqzrMeK81d
Aov6WuWk6UF43hdS7ibwUWJBQ4Kj4KwIIH8DSa2Nch3SMgY8na7tqabPa0pG33yc
w5GsPgFAdQKBgQCIh9L/BExlKTajK7uVJMekuRF5Kl+RSYsyZ2zzk8yD9bXJmyPf
jwcCKVKbomAwe46EhYC/SQEvIxs08teJp3+IUIWzgXmZ52fW/jy7Y7lk96qH0ahE
cECsmIYFw2xZYeHLMpBfJjAdDnAscsfTtrIv/K/l8xj4NfC8EKH7A0hr0QKBgApq
T5VNrsC+odjr+8su94GV0T+B9f4FBjdWipnGxUTJhaQUx/NxdxbtkNy+vP9Oeftx
AQ4CD3asMAXIJiB3rmN49vGtPXPgijWZo98slmURvcyfXKm0lb9Pnm78b8OnAYNT
uXv22pSsy7YCPoZgvEdBUZmKIyuS0GknPsy1DSvtAoGBALL8h9XJhlZMMLJJGUN0
dtxPlDqfHAJg8MGCT4G/r0WXbdVJ+bbGH2uURmvvS+iK0CVH6ztgf4WxUncwr0aa
RJ9+RRV7VEo2MDMEWwjArSAzfwWNFU2x6CbGIaw/A/iMUvS3QrN6sO64R4EF4e2n
f3PbOm7m2F6tQAKE89fxq8uX
-----END PRIVATE KEY-----
</key>
<dh>
-----BEGIN DH PARAMETERS-----
MIIBCAKCAQEAngYiw32VVwxCzvdWnShP9p3TNEI/aLNqrGzH/HVeKHZpK/iNlmyD
vjn5K/nLv24GRl+TbubQ/pGRPVtBaBX3T+t9Jb8zuougdA4UCegM1KtinmpU+bI1
DbM7bd75VzB9mBMjo6F+Vc8AFIeKnf/+iWLHsdavU80p7FquLOaq0sFMPwjpilxC
1rcX3K+lf1ZId8hgHX3hgR+7MKRYEd97g8OI9soXNkas7YgsCxWG/WmEGf7Md2HS
/cYaweFO4qedTz1ZKgbidqvCy95EzhcrA5plyNkQFowQzWyrGuOYBrVPN3xis8o8
+t+yeu7AgO62RkueCKFD7ibO5eumJ6fYJwIBAg==
-----END DH PARAMETERS-----
</dh>
EOF
systemctl start openvpn-server@server
systemctl enable openvpn-server@server
# 为所有转发的流量设置SNAT
iptables -t nat -A POSTROUTING -o eth0 ! -s 172.16.11.0/24 -j MASQUERADE
# 在客户端同时做SNAT的情况下，上面的配置可以简化为
# iptables -t nat -A POSTROUTING -o eth0 -s 10.200.0.0/24 -j MASQUERADE
```

启动客户端

```bash
# 要在日志文件里打时间戳
sed -i 's/ --suppress-timestamps//g' /usr/lib/systemd/system/openvpn-client@.service
systemctl daemon-reload
cat > /etc/openvpn/client/client.conf << 'EOF'
dev tap
proto udp
remote openvpn.lgypro.com 1194
client
remote-cert-tls server
user openvpn
group openvpn
verb 7
daemon
log-append /var/log/openvpn.log
<tls-crypt>
#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
371230b610ebb21082c6c9d6b33ce479
0373763e596a207d9505ba82d2f9cd00
02d0afff7a0dad5fb32e04e881215d39
9a707ce8057d7e211b0e528ecbded2f3
7588eaa555cad5e043f9a6ac02243d61
95cce2f3eaa319a77bf41fb48f7a5690
2bc63dc7e0d356c710fd5302ec944d70
ece2d60caee95a9e63dd7240ca570f9d
04ccfbaea207a8c25852e16184aa30f5
72107dd002d20e4df69d62f330bb4953
e8a2c26200f42438b2458114cc630e24
d7812a701a54d1fe963b240997887dce
fa43c6470e6093dd2e08e8bd191fc318
431452e91e04789dc533cbbc70420672
83915d3b2392d886de300146eae8277a
35aa3c8a549b35c08d0ca63183251f84
-----END OpenVPN Static key V1-----
</tls-crypt>
<ca>
-----BEGIN CERTIFICATE-----
MIIDSDCCAjCgAwIBAgIUOn5EavRhh0MjROvEX406VZDUDOkwDQYJKoZIhvcNAQEL
BQAwFTETMBEGA1UEAwwKT1BFTlZQTi1DQTAeFw0yNDAxMDYwMjQxMTFaFw0zNDAx
MDMwMjQxMTFaMBUxEzARBgNVBAMMCk9QRU5WUE4tQ0EwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQDvS15NQpMqFPCSkBbEYSYiYAeXkA8KvAFwDQ6fwgwo
9yctWk6c4AGNy7NG/rsaZjngUywPVftR6YXrWIo+iobPknSocjic/j7vAxCkbZAC
wQkWCtQgp1n3IoPGNB2wZQ7eZSX14xk3GhjsdupH7j7oPuC6hLxcoWEy1RYvoa1R
2OsXg4ng7vQNJaHDk5qyD/pZRv8IudqRRrDbwuTbKD2fmilO7e+BmjCsSs5VocaV
hGnqcb/j2TgesESfPjUMxQz4NxcgU9+Rcx+UQ3yDZwh3OB90A+qKF6sGe0UIVsR1
5M9TuZZCLalZELKhWRO7UxGDBStbdUqz18dl2omrR/mzAgMBAAGjgY8wgYwwHQYD
VR0OBBYEFN5mqGBGtsFPXPNnksgRE700HV0yMFAGA1UdIwRJMEeAFN5mqGBGtsFP
XPNnksgRE700HV0yoRmkFzAVMRMwEQYDVQQDDApPUEVOVlBOLUNBghQ6fkRq9GGH
QyNE68RfjTpVkNQM6TAMBgNVHRMEBTADAQH/MAsGA1UdDwQEAwIBBjANBgkqhkiG
9w0BAQsFAAOCAQEA154qeblSiRR6s2bHD5JmXWZ4s+o8n3H9Gz46XBsq8WAMZjuC
XMlzocrpkFMM8r8rWtsAbGqMAkBf1lLCVGMKIHzayfBpRnJ5VOIFY7aESUqGfqCc
s2JMH9meo69cWnwcNPK4PPrgYAC3+UxV9YrvdwyeQUGvbh2ybIiW1M3j7BXBcGKE
/xq0eXoHrjQu2443YsXnwYmdCe7YAPs8QTp+un6FWWIhSIT6/rvmbW6MsndQTyj/
tXLDnqLUVFfM1l8LGqqpFx8Y8buf/1vMdSstnWi7+X/ZBe5nPDCdjQJUa4cWbkxD
MPj3c7Co9WIrCGW/K1PTY0VqUqzeua6DNSAcJw==
-----END CERTIFICATE-----
</ca>
<cert>
-----BEGIN CERTIFICATE-----
MIIDVDCCAjygAwIBAgIRAJTZv3KybSLilvn+fDuM+dwwDQYJKoZIhvcNAQELBQAw
FTETMBEGA1UEAwwKT1BFTlZQTi1DQTAeFw0yNDAxMDYwMjQzMTlaFw0yNjA0MTAw
MjQzMTlaMBIxEDAOBgNVBAMMB2NsaWVudDEwggEiMA0GCSqGSIb3DQEBAQUAA4IB
DwAwggEKAoIBAQC1Rp9T0x9fWowKs3hPc9m8CX+EmzhuiFYhqfpDy93vuZ1/zXFw
U0XK4t6SAZDqkX5dCqgfELkhDkqSYO+80iRXU2SCKTFFBqMtosq27PM1VUBxac6F
FLkn5zqkEc5KnBC9TEQkhLtGmT+QBddwoh4BxZY1jMwhXQ8PVo7DgXKw0+ldDMFB
DOIsfejzYPyXmhdmygGCuBv6CUo6dPMyPXJq2vKiZmaF1r/9opaJoFs/WXDt4wQh
ry1KweQqs5mtv+HkRreOdtEc1hQLj7umV/iolS48VcGb0nFRtbfcY2msao5maJr0
47vGnao/tu3RPTNLaUTw0ZF/2hyFYZp0sNQxAgMBAAGjgaEwgZ4wCQYDVR0TBAIw
ADAdBgNVHQ4EFgQUzQ8L6/SKR6TDdffYWtm3ea2yHDcwUAYDVR0jBEkwR4AU3mao
YEa2wU9c82eSyBETvTQdXTKhGaQXMBUxEzARBgNVBAMMCk9QRU5WUE4tQ0GCFDp+
RGr0YYdDI0TrxF+NOlWQ1AzpMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAsGA1UdDwQE
AwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAbF602WT+Lb7s7AMzL80t+HZzV6OvjQOE
q/uxaX3s/WXwtMUItD+tz/5+9wfGaGhQOyXl3WJC4Mf6qjEuH8YudnYE/hmNE0yQ
iSGoF+D14zNVuBXtT13v2zuzcBx7AsCGtw2RAvxKw4VndkKyYvniQPLIJZocbG82
UqwqymFdEgjY1VSX85CLAqS82eIPRYsfFdE0WJdkfRZpgW0Yjhf+qgeh3sl2S43a
Vep+bcYkURWwjguA+V++umodfn6BhIxzSqmCa7A+ar6kxcgUloAR5fhSbBCjExeT
i1TR11t9nIlPK1j+Tg0nERuXQ0vPWNG2BBIRwC8fh4bAalc00slTCw==
-----END CERTIFICATE-----
</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC1Rp9T0x9fWowK
s3hPc9m8CX+EmzhuiFYhqfpDy93vuZ1/zXFwU0XK4t6SAZDqkX5dCqgfELkhDkqS
YO+80iRXU2SCKTFFBqMtosq27PM1VUBxac6FFLkn5zqkEc5KnBC9TEQkhLtGmT+Q
Bddwoh4BxZY1jMwhXQ8PVo7DgXKw0+ldDMFBDOIsfejzYPyXmhdmygGCuBv6CUo6
dPMyPXJq2vKiZmaF1r/9opaJoFs/WXDt4wQhry1KweQqs5mtv+HkRreOdtEc1hQL
j7umV/iolS48VcGb0nFRtbfcY2msao5maJr047vGnao/tu3RPTNLaUTw0ZF/2hyF
YZp0sNQxAgMBAAECggEACzJDTqpIZ/DP8SyYPpFhaOejRyhOf3vklKwK0kYqEo+X
MUNvVr3l89E4aFyNZqzkLlClWqnYmA3pwDRu35g7zg2ktGoL6ioDSSMivH1SdkBU
5yG0XkGN6sTcOTGvrqmzWlB2wv9UZoNk9XskN9zjgpOBeBRBWsGEgnDZB4KcIrIR
ZiXtDz+LizQozNOUSBva1pI7ng1y7RZ0AcnsLW6pD6eE9u/EVeg5qYybCP9HzCRP
7dPqUVITJqbwITY3fpfeJdFcyJd0ZfDd4sohJXdFVKspZCnP4EfEOLr+cnFMi6M7
Vz7hYRAsRZ8vnJuyv1IpEjpZQZ0XCWYbP/CV8kWfmQKBgQDSKANbgR75V1VQ23ok
k9FNWJ0ebAl88t5UEKUA28clWjPOCTyNWDPjidTzUNt6J1rZ2xH+dGJg2kdXokcS
Xh6y6spEzQz41po14QtAwnMfg1wuaL7ttbgOSaJBMxtlTL28AX6GL7l3A/BvVZ9n
RQ6Vv1IO1e7SpvXTCAeZq48NDQKBgQDc0c7PYyeciNIz9Vi/2rWehvzHPkog2Axg
k5+cB6DoGP74bOOGdHyCjh3mKKHrIiCsi+Klvg5LZbYLIHYDOmC+dnnkGgjRrroy
k3V8G10pQgfYOddlpg9O+8WkNhaikkNX3xkIHU1ulou3n3nxIhNVI+bXaRXz7jTU
PlGL0JyCtQKBgBsUluIqsFAXBeJmdcXS2myqF4z9BPf539ZbUrfamj3g8r4BCLF6
BS2z7CHycGW8PoPGK/prCuZKGllbiub+A9ywTqIw/hPuq2538lhE9krARZehXcEJ
4o7MxEC8kjIqgmSAmMo3yiFg+5GNKf5HsspvHebVgHHam+C2rywJJGTdAoGAUInZ
czyH8wjYGglPQFJl0ZcMVSM76DTEdukA2ujRYDXVsQgOCDkuPHPXJd4GCHufDS/M
D4V9MzS6q95ADLdbF7yggniYZNnkoZO00vosBWNG9y9Jh5KEnNspX9Y2dT8Bfugo
+hOt7TrNZMCuuisif/gjNsfmMNzdudes6GDC5M0CgYEAmvp4l30ERrZgA1eDo/hl
txow+0/BV88omo6nmgLYGrxxofyvDoMB4Y0EXSCJu6echyKzkpf0i/8scCgb7eqS
rPJykgLwvH25Yo4pfFDgCAtRAftRzECdyVHMtFnyWlWT3qje1fgn1Qx3aYGvRltv
pGmwSnplLPcelfv5U21aCnY=
-----END PRIVATE KEY-----
</key>
EOF
systemctl start openvpn-client@client.service
systemctl enable openvpn-client@client.service
# 这里也要做SNAT，是为了让vpn服务器认为所有的流量都是从vpn客户端的tap接口发出的，从而在回包的过程中，不用新建路由
# 新增一个客户端时，不需要vpn服务器做任何路由层面的改动
iptables -t nat -A POSTROUTING -o tap0 ! -s 10.200.0.0/24 -j MASQUERADE
```

### 测试

在mock-client上执行

```bash
ping -c 3 172.16.12.1
ping -c 3 172.16.11.1
ping -c 3 172.16.11.100
```

## 进一步思考

如何把设备类型，从dev=tap改成dev=tun会怎样？

把上面两个配置文件里的dev=tap改成dev=tun，重启服务。删掉原有的iptables规则，新增下面的，理论上能达到同样的效果。

```bash
# 在服务器上，和dev=tap时使用的一样
iptables -t nat -A POSTROUTING -o eth0 ! -s 172.16.11.0/24 -j MASQUERADE
# 在客户端上，和dev=tap时使用的不一样，要用新的网卡名称
iptables -t nat -A POSTROUTING -o tun0 ! -s 10.200.0.0/24 -j MASQUERADE
```

在实际测试中，发现情况并不像预想的那样。当停掉原有的openvpn进程，修改配置，重启，
重新添加iptables规则后，发现在vpn客户端上nat表的POSTROUTING链里的MASQUERADE规则并没有生效，
导致从mock-client发往mock-server的数据包，没有在vpn-client发生地址转换，从而在到达vpn服务器后被直接丢弃。

```log
Tue Jan  9 19:24:33 2024 us=262695 client1/39.98.124.215:33684 MULTI: bad source address from client [172.16.12.234], packet dropped
Tue Jan  9 19:24:34 2024 us=261788 client1/39.98.124.215:33684 MULTI: bad source address from client [172.16.12.234], packet dropped
Tue Jan  9 19:24:35 2024 us=911271 client1/39.98.124.215:33684 MULTI: bad source address from client [172.16.12.234], packet dropped
Tue Jan  9 19:24:36 2024 us=910767 client1/39.98.124.215:33684 MULTI: bad source address from client [172.16.12.234], packet dropped
Tue Jan  9 19:24:37 2024 us=910840 client1/39.98.124.215:33684 MULTI: bad source address from client [172.16.12.234], packet dropped
Tue Jan  9 19:24:58 2024 us=449436 client1/39.98.124.215:33684 MULTI: bad source address from client [172.16.12.234], packet dropped
Tue Jan  9 19:24:59 2024 us=448817 client1/39.98.124.215:33684 MULTI: bad source address from client [172.16.12.234], packet dropped
Tue Jan  9 19:25:00 2024 us=448766 client1/39.98.124.215:33684 MULTI: bad source address from client [172.16.12.234], packet dropped
Tue Jan  9 19:25:03 2024 us=464782 client1/39.98.124.215:33684 MULTI: bad source address from client [172.16.12.234], packet dropped
Tue Jan  9 19:25:04 2024 us=463910 client1/39.98.124.215:33684 MULTI: bad source address from client [172.16.12.234], packet dropped
Tue Jan  9 19:25:05 2024 us=463793 client1/39.98.124.215:33684 MULTI: bad source address from client [172.16.12.234], packet dropped
```

排查不到原因，重启现有的vpn-server、vpn-client，重新配置一次，就可以了。